# i3wm

Haskell bindings for the i3 window manager's inter-process communication (IPC) channel, allowing you to communicate with i3 over a unix socket.

## Manipulating IPC responses

As certain IPC responses can be deeply nested or feature recursive types, I've included label prism and lens instances for each datatype using [Optics](https://hackage.haskell.org/package/optics). This lets you traverse structures more easily and overcomes some of the issues using duplicate record field names in Haskell. To use the optics interface, import `Optics` and enable the `OverloadedLabels` language extension.

For instance:

```haskell
{-# LANGUAGE OverloadedLabels #-}

import System.I3.IPC (connectIPC, getTree)
import Optics -- or `Optics.Core` if using optics-core

main :: IO ()
main = do
    s <- connectIPC
    tree <- getTree s -- gets the entire, recursive layout tree
    -- let's print the height of the last child node of the root node
    print $ tree ^? _Right % #_Tree % #nodes % _last % #rect % #height
    -- Just 1280

```

I thought this might be a more convenient method of dealing with deeply nested IPC responses.
