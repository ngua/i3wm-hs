{-# LANGUAGE DisambiguateRecordFields #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE ViewPatterns #-}

-- |
-- Module         : System.I3.IPC
-- Copyright      : (c) Rory Tyler Hayford, 2020
-- License        : BSD-3-Clause
-- Maintainer     : rory(dot)hayford(at)protonmail(dot)com
-- Stability      : Experimental
-- Portability    : Non-portable
--
-- Operations for i3's IPC
--
module System.I3.IPC
    (  -- * Primitive actions
       -- $prim
      i3SocketPath
    , connectIPC
    , recvResponse
    , recvReply
    , recvEvent
    , recvReplyEither
    , recvEventEither
    , recvResponseEither
      -- * Utility functions
      -- $util
    , findNode
    , findFocusedNode
    , findNodes
      -- * Utility IPC actions
      -- $actions
    , runCommand
    , getWorkspaces
    , subscribe
    , subscribeEither
    , getTree
    , getMarks
    , getBarConfig
    , getBarIDs
    , getVersion
    , getBindingModes
    , getConfig
    , sendTick
    , sync
    , getBindingState
    ) where

import           Control.Applicative          ( Alternative((<|>)) )
import           Control.Exception            ( try )
import           Control.Monad                ( forever, void )
import           Control.Monad.Catch
                 ( MonadMask
                 , MonadThrow
                 , bracket
                 , throwM
                 )
import           Control.Monad.IO.Class       ( MonadIO(liftIO) )

import           Data.Bits                    ( Bits(clearBit, testBit) )
import           Data.ByteString              ( ByteString )
import           Data.Sequence                ( (><), (|>), Seq((:<|), Empty) )
import qualified Data.Sequence                as Seq
import           Data.Serialize               ( getWord32le, runGet )
import           Data.Text                    ( Text )

import           Network.Socket.ByteString    ( recv )

import           Prelude                      hiding ( id )

import           System.I3.IPC.Error          ( IpcError(..), fromString )
import           System.I3.IPC.Message        as Reply ( Reply, decodeReply )
import qualified System.I3.IPC.Message        as Message
import           System.I3.IPC.Message.Types  ( ClientMessage )
import           System.I3.IPC.Response       ( Response )
import qualified System.I3.IPC.Response       as Response
import           System.I3.IPC.Socket
import           System.I3.IPC.Subscribe      ( Event, Subscription )
import qualified System.I3.IPC.Subscribe      as Event ( decodeEvent )
import           System.I3.IPC.Types          ( Name, Node )
import qualified System.I3.IPC.Types.Internal
import           System.I3.IPC.Types.Internal ( Node(Node) )

-----------------------------------------------------------------------------
-- Utilities
-----------------------------------------------------------------------------
--
-- | Given a 'Node', find the first one that satisfies a given predicate
findNode :: (Node -> Bool) -> Node -> Maybe Node
findNode p node@(p -> True) = Just node
findNode p Node { nodes, floatingNodes } = go (nodes >< floatingNodes)
  where
    go Empty           = Nothing
    go (next :<| rest) = findNode p next <|> go rest

-- | Walk a tree of 'Node's to find the focused one by following the head of
--  the parent's @focus@ field
findFocusedNode :: Node -> Maybe Node
findFocusedNode node@Node { focus, focused, nodes, floatingNodes }
    | focused = Just node
    | otherwise = go focus (nodes >< floatingNodes)
  where
    go Empty _          = Nothing
    go (first :<| _) ns = compareID first ns

    compareID _ Empty = Nothing
    compareID f (next@Node { id } :<| rest)
        | id == f = findFocusedNode next
        | otherwise = compareID f rest

-- | Recursively find all nodes that satisfy a given predicate
findNodes :: (Node -> Bool) -> Node -> Seq Node
findNodes p node = go (Seq.singleton node) Seq.empty
  where
    go (next@Node { nodes, floatingNodes } :<| rest) ns
        | p next = go (nodes >< floatingNodes) (go rest ns) |> next
        | otherwise = go (nodes >< floatingNodes) (go rest ns)
    go _ ns = ns

-----------------------------------------------------------------------------
-- Primitive actions
-----------------------------------------------------------------------------
--
-- | Read raw bytes from the IPC socket, getting the type of the response
-- and its body
rawResponse :: (MonadIO m, MonadThrow m) => I3Socket -> m (Int, ByteString)
rawResponse (I3Socket soc) = liftIO $ recv soc 6 >>= \case
    "i3-ipc" -> do
        len <- liftIO $ recv soc 4 >>= recvInt
        ty <- liftIO $ recv soc 4 >>= recvInt
        body <- liftIO $ recv soc len
        return (ty, body)
    _        -> throwM $ SerializationError "Couldn't read IPC response"
  where
    recvInt bs = either (throwM . fromString)
                        (return . fromIntegral)
                        (runGet getWord32le bs)

-- | Deserialize IPC response into 'Response', depending on value of highest bit
recvResponse :: (MonadIO m, MonadThrow m) => I3Socket -> m Response
recvResponse soc = rawResponse soc >>= \(ty, body) ->
    let res = if testBit ty highBit
              then Response.Subscribe <$> Event.decodeEvent (stripBit ty) body
              else Response.Message <$> decodeReply ty body
    in
        either throwM return res

-- | Parse IPC response directly into 'Reply', if type of response is certain
recvReply :: (MonadIO m, MonadThrow m) => I3Socket -> m Reply
recvReply soc = rawResponse soc >>= \raw ->
    either throwM return (uncurry decodeReply raw)

-- | Parse IPC response directly into 'Event', if type of response is certain
recvEvent :: (MonadIO m, MonadThrow m) => I3Socket -> m Event
recvEvent soc = rawResponse soc >>= \(ty, body) ->
    either throwM return (Event.decodeEvent (stripBit ty) body)

-- | Convert raw response to an 'Either'
rawEither :: (MonadIO m) => I3Socket -> m (Either IpcError (Int, ByteString))
rawEither = liftIO . try @IpcError . rawResponse

-- | Variant of 'recvResponse' that produces an 'Either' rather than throwing
-- an exception
recvResponseEither
    :: (MonadIO m, MonadThrow m) => I3Socket -> m (Either IpcError Response)
recvResponseEither soc = rawResponse soc >>= \(ty, body) -> return
    $ if testBit ty highBit
      then Response.Subscribe <$> Event.decodeEvent (stripBit ty) body
      else Response.Message <$> decodeReply ty body

-- | Variant of 'recvReply' that produces an 'Either' rather than throwing
-- an exception
recvReplyEither :: (MonadIO m) => I3Socket -> m (Either IpcError Reply)
recvReplyEither soc = rawEither soc
    >>= either (return . Left) (return . uncurry decodeReply)

-- | Variant of 'recvEvent' that produces an 'Either' rather than throwing
-- an exception
recvEventEither :: (MonadIO m) => I3Socket -> m (Either IpcError Event)
recvEventEither soc = rawEither soc
    >>= either (return . Left)
               (\(ty, body) -> return $ Event.decodeEvent (stripBit ty) body)

-- | Subscribe to an 'Event' by providing a list of 'Subscription' types
-- and a handler action
subscribe
    :: (MonadIO m, MonadMask m) => (Event -> m ()) -> [Subscription] -> m ()
subscribe handler ss = bracket connectIPC (liftIO . closeIPC) loop
  where
    loop soc = forever . void
        $ Message.send soc (Message.Subscribe ss)
        >> recvReply soc
        >> recvEvent soc
        >>= handler

-- | Subscribe to an 'Event' by providing a list of 'Subscription' types
-- and a handler action; 'Either' variant
subscribeEither :: (MonadIO m, MonadMask m)
                => (Either IpcError Event -> m ())
                -> [Subscription]
                -> m ()
subscribeEither handler ss = bracket connectIPC (liftIO . closeIPC) loop
  where
    loop soc = forever . void
        $ Message.send soc (Message.Subscribe ss)
        >> recvReply soc
        >> recvEventEither soc
        >>= handler

-----------------------------------------------------------------------------
-- Utility/convenience actions
-----------------------------------------------------------------------------
--
-- | Run one or more commands
runCommand :: (MonadIO m) => I3Socket -> [Text] -> m (Either IpcError Reply)
runCommand soc commands = Message.send soc (Message.RunCommand commands)
    >> recvReplyEither soc

-- | Get list of 'System.I3.IPC.Message.Reply.Workspace's
getWorkspaces :: (MonadIO m) => I3Socket -> m (Either IpcError Reply)
getWorkspaces soc =
    Message.send soc Message.GetWorkspaces >> recvReplyEither soc

-- | Get layout tree of 'System.I3.IPC.Types.Internal.Node's
getTree :: (MonadIO m) => I3Socket -> m (Either IpcError Reply)
getTree soc = Message.send soc Message.GetTree >> recvReplyEither soc

-- | Get names of currently set 'System.I3.IPC.Message.Reply.Mark's
getMarks :: (MonadIO m) => I3Socket -> m (Either IpcError Reply)
getMarks soc = Message.send soc Message.GetMarks >> recvReplyEither soc

-- | Get all bar IDs. Note that "ID" in this context refers to a unique string name,
-- not a numerical ID
getBarIDs :: (MonadIO m) => I3Socket -> m (Either IpcError Reply)
getBarIDs soc = Message.send soc (Message.GetBarConfig Nothing)
    >> recvReplyEither soc

-- | Get 'System.I3.IPC.Types.Internal.BarConfig' object given a specific bar's string ID
getBarConfig :: (MonadIO m) => I3Socket -> Name -> m (Either IpcError Reply)
getBarConfig soc bid = Message.send soc (Message.GetBarConfig $ Just bid)
    >> recvReplyEither soc

-- | Get the i3 version as a 'System.I3.IPC.Message.Reply.VersionReply'
getVersion :: (MonadIO m) => I3Socket -> m (Either IpcError Reply)
getVersion soc = Message.send soc Message.GetVersion >> recvReplyEither soc

-- | Get configured 'System.I3.IPC.Message.Reply.BindingMode's
getBindingModes :: (MonadIO m) => I3Socket -> m (Either IpcError Reply)
getBindingModes soc =
    Message.send soc Message.GetBindingModes >> recvReplyEither soc

-- | Get the contents of currently loaded 'System.I3.IPC.Message.Reply.Config'
getConfig :: (MonadIO m) => I3Socket -> m (Either IpcError Reply)
getConfig soc = Message.send soc Message.GetConfig >> recvReplyEither soc

-- | Send tick with an arbitrary payload
sendTick :: (MonadIO m) => I3Socket -> Text -> m (Either IpcError Reply)
sendTick soc str = Message.send soc (Message.SendTick str)
    >> recvReplyEither soc

-- | Send sync request with a properly configured 'System.I3.IPC.Message.Types.ClientMessage'
sync :: (MonadIO m) => I3Socket -> ClientMessage -> m (Either IpcError Reply)
sync soc cm = Message.send soc (Message.Sync cm) >> recvReplyEither soc

-- | Get the currently active 'System.I3.IPC.Message.Reply.BindingMode'
getBindingState :: (MonadIO m) => I3Socket -> m (Either IpcError Reply)
getBindingState soc =
    Message.send soc Message.GetBindingState >> recvReplyEither soc

-- | Remove the highest bit from the IPC response
stripBit :: Bits a => a -> a
stripBit = flip clearBit highBit

-- | Highest bit in IPC response, indicating its type ('Reply' or 'Event')
highBit :: Int
highBit = 31
-- $actions
-- All @get*@ actions, 'runCommand', and 'subscribe'\/'subscribeEither' are utility actions that can
-- be constructed from the primitives in this module. These correspond to 'System.I3.IPC.Message.Types.MessageType's
-- that can be sent as one-time messages to i3, e.g.:
--
-- >>> s <- connectIPC
-- >>> getBindingState s
-- Right (BindingState (BindingStateReply {name = Name "default"}))
--
-- Note that this is equivalent to
--
-- >>> Message.send s Message.GetBindingState >> recvReplyEither s
--
-- If you don't want to perform a sophisticated subscription action (e.g. with streaming), 'subscribe'
-- allows you to observe IPC events and run an IO action whenever the socket receives an event, e.g.:
--
-- >>> subscribe print [Subscribe.Binding]
-- Bindings (BindingEvent
--           { change  = "run"
--           , binding = BindingDetails
--                 { command        = "workspace 2"
--                 , eventStateMask = fromList [ "Mod4" ]
--                 , inputCode      = 0
--                 , symbol         = Just "2"
--                 , inputType      = Keyboard
--                 }
--           })
-- ^CInterrupted.
--
--
-- $util
--
-- These functions are provided for convenience when processing IPC responses. At the moment, this only
-- consists of functions for finding 'Node's within the layout tree, although more may be added in the
-- the future.
--
--
-- $prim
-- These provide relatively low-level actions to communicate with i3, and can be composed
-- to replicate all of the convenience utility actions below.
--
-- Opening a socket connection is simple:
--
-- >>> s <- connectIPC
--
-- This provides an 'I3Socket'.
--
-- Responses received from i3 can be parsed into a 'Response' if the response type is not
-- known, or directly into an 'System.I3.IPC.Subscribe.Event' for subscribed events or a
-- 'System.I3.IPC.Message.Reply'
--
