{-# LANGUAGE DeriveGeneric #-}

-- | Error type for IPC actions
module System.I3.IPC.Error where

import           Control.Exception ( Exception )

import           Data.Text         ( Text )
import qualified Data.Text         as T
import           Data.Typeable     ( Typeable )

import           GHC.Generics      ( Generic )

import           Optics.TH

-- | Error type for IPC actions
data IpcError
    = -- | Error when communicating over socket
      ConnectionError Text
      -- | Error serializing (sending/receiving) data types over IPC
    | SerializationError Text
    deriving ( Show, Eq, Generic, Typeable )

instance Exception IpcError

-- | Converts 'String' to 'SerializationError' containing 'Text', useful for 'Data.Aeson.eitherDecode',
-- which produces an @Either String a@
fromString :: String -> IpcError
fromString = SerializationError . T.pack

makePrismLabels ''IpcError
