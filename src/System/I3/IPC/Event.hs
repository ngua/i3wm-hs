-- | IPC events to which users may subscribe
module System.I3.IPC.Event
    ( Event(..)
    , WorkspaceEvent
    , WorkspaceChange
    , OutputEvent
    , ModeEvent
    , WindowChange
    , WindowEvent
    , BarconfigEvent
    , BindingEvent
    , BindingDetails
    , ShutdownEvent
    , TickEvent
    , decodeEvent
    ) where

import           Data.Aeson                ( eitherDecodeStrict' )
import           Data.Bifunctor            ( Bifunctor(bimap) )
import           Data.ByteString           ( ByteString )

import           System.I3.IPC.Error       ( IpcError(SerializationError)
                                           , fromString
                                           )
import           System.I3.IPC.Event.Types

-- | Fallibly decode i3 IPC response into an 'Event'
decodeEvent :: Int -> ByteString -> Either IpcError Event
decodeEvent 0 bs = bimap fromString Workspace (eitherDecodeStrict' bs)
decodeEvent 1 bs = bimap fromString Output (eitherDecodeStrict' bs)
decodeEvent 2 bs = bimap fromString Mode (eitherDecodeStrict' bs)
decodeEvent 3 bs = bimap fromString Window (eitherDecodeStrict' bs)
decodeEvent 4 bs = bimap fromString BarconfigUpdate (eitherDecodeStrict' bs)
decodeEvent 5 bs = bimap fromString Binding (eitherDecodeStrict' bs)
decodeEvent 6 bs = bimap fromString Shutdown (eitherDecodeStrict' bs)
decodeEvent 7 bs = bimap fromString Tick (eitherDecodeStrict' bs)
decodeEvent _ _  = Left . SerializationError $ "Unknown event"
