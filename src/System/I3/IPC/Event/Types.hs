{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE StrictData #-}

-- | Types representing IPC responses to subscribed events
module System.I3.IPC.Event.Types
    ( Event(..)
    , WorkspaceEvent(..)
    , WorkspaceChange(..)
    , OutputEvent(..)
    , ModeEvent(..)
    , WindowChange(..)
    , WindowEvent(..)
    , BarconfigEvent
    , BindingEvent(..)
    , BindingDetails(..)
    , ShutdownEvent(..)
    , TickEvent(..)
    ) where

import           Data.Aeson
import           Data.Aeson.Casing   ( snakeCase )
import           Data.Char
import           Data.Sequence       ( Seq )
import           Data.Text           ( Text )
import qualified Data.Text           as T
import           Data.Text.Optics

import           GHC.Generics        ( Generic )

import           Optics

import           Prelude             hiding ( id )

import           System.I3.IPC.Types

-- | Available event types for subscription
data Event
    = -- | Sent when the user switches to a different workspace, when a new workspace is
      -- initialized or when a workspace is removed (because the last client vanished).
      Workspace WorkspaceEvent
      -- | Sent when RandR issues a change notification (of either screens, outputs,
      -- CRTCs or output properties).
    | Output OutputEvent
      -- | Sent whenever i3 changes its binding mode.
    | Mode ModeEvent
      -- | Sent when a client’s window is successfully reparented (that is when i3 has
      -- finished fitting it into a container), when a window received input focus or
      -- when certain properties of the window have changed.
    | Window WindowEvent
      -- | Sent when the hidden_state or mode field in the barconfig of any bar instance
      -- was updated and when the config is reloaded.
    | BarconfigUpdate BarconfigEvent
      -- | Sent when a configured command binding is triggered with the keyboard or mouse
    | Binding BindingEvent
      -- | Sent when the ipc shuts down because of a restart or exit by user command
    | Shutdown ShutdownEvent
      -- | Sent when the ipc client subscribes to the tick event (with "first": true)
      -- or when any ipc client sends a @SEND_TICK@ message (with "first": false).
    | Tick TickEvent
    deriving ( Show, Eq, Generic )

instance FromJSON Event where
    parseJSON = genericParseJSON snakeCaseConstructorOptions

-- | This event consists of a single serialized map containing a property change (string)
-- which indicates the type of the change ("focus", "init", "empty", "urgent", "reload",
-- "rename", "restored", "move"). A current (object) property will be present with the
-- affected workspace whenever the type of event affects a workspace (otherwise, it will be null).
-- When the change is "focus", an old (object) property will be present with the previous workspace.
-- When the first switch occurs (when i3 focuses the workspace visible at the beginning) there
-- is no previous workspace, and the old property will be set to null. Also note that if the previous
-- is empty it will get destroyed when switching, but will still be present in the "old" property.
data WorkspaceEvent = WorkspaceEvent
    { id      :: ID
    , type'   :: WorkspaceChange
    , current :: Node
    , old     :: Maybe Node
    }
    deriving ( Show, Eq, Generic )

instance FromJSON WorkspaceEvent where
    parseJSON = genericParseJSON defaultOptions
                                 { fieldLabelModifier = filter ('\'' /=)
                                 }

-- | The type of workspace change
data WorkspaceChange
    = FocusWS
    | InitWS
    | EmptyWS
    | UrgentWS
    | ReloadWS
    | RenameWS
    | RestoredWS
    | MoveWS
    deriving ( Show, Eq, Generic )

instance FromJSON WorkspaceChange where
    parseJSON = genericParseJSON defaultOptions
                                 { constructorTagModifier = stripSuffix
                                 }
      where
        stripSuffix s = s & packed %~ T.dropEnd 2 & snakeCase

-- | Consists of a single [...] property, change (string), which indicates the type of
-- the change (currently only "unspecified").
data OutputEvent = OutputEvent { change :: Text }
    deriving ( Show, Eq, Generic )

instance FromJSON OutputEvent where
    parseJSON = genericParseJSON defaultOptions

-- | Name of current mode in use
data ModeEvent = ModeEvent
    { -- | Name of mode, the same as specified in config when creating a mode.
      -- The default mode is simply named default.
      mode        :: Name
      -- | Whether pango markup shall be used for displaying this mode
    , pangoMarkup :: Bool
    }
    deriving ( Show, Eq, Generic )

instance FromJSON ModeEvent where
    parseJSON = genericParseJSON snakeCaseFieldOptions

-- | Consists of a single [..] property, change (string), which indicates the type of the change
data WindowEvent = WindowEvent { change :: WindowChange }
    deriving ( Show, Eq, Generic )

instance FromJSON WindowEvent where
    parseJSON = genericParseJSON defaultOptions

-- | Type of window change
data WindowChange
    = New -- ^ Window has become managed by i3
    | Close -- ^ Window has closed
    | Focus -- ^ Window has has received input focus
    | Title -- ^ Window’s title has changed
    | FullscreenMode -- ^ Window has entered or exited fullscreen mode
    | Move -- ^ Window has changed its position in the tree
    | Floating -- ^ Window has transitioned to or from floating
    | Urgent -- ^ Window has become urgent or lost its urgent status
    | Mark -- ^ A mark has been added to or removed from the window
    deriving ( Show, Eq, Generic )

instance FromJSON WindowChange where
    parseJSON = genericParseJSON defaultOptions
                                 { constructorTagModifier = (toLower <$>)
                                 }

-- | Options from the barconfig of the specified bar_id that were updated in i3. This
-- event is the same as a @GET_BAR_CONFIG@ reply for the bar with the given id.
type BarconfigEvent = BarConfig

-- | Details of a binding that ran a command because of user input
data BindingEvent = BindingEvent
    { -- | Indicates what sort of binding event was triggered (right now it will always
      -- be "run" but may be expanded in the future).
      change  :: Text
      -- | Details about the binding that was run
    , binding :: BindingDetails
    }
    deriving ( Show, Eq, Generic )

instance FromJSON BindingEvent where
    parseJSON = genericParseJSON defaultOptions

-- | Mouse or keyboard binding that was run
data BindingDetails = BindingDetails
    { -- | The i3 command that is configured to run for this binding.
      command        :: Text
      -- | The group and modifier keys that were configured with this binding.
    , eventStateMask :: Seq Text
      -- | If the binding was configured with bindcode, this will be the key code that
      -- was given for the binding. If the binding is a mouse binding, it will be the
      -- number of the mouse button that was pressed. Otherwise it will be 0.
    , inputCode      :: Int
      -- | If this is a keyboard binding that was configured with bindsym, this field
      -- will contain the given symbol. Otherwise it will be null.
    , symbol         :: Maybe Text
      -- | This will be "keyboard" or "mouse" depending on whether or not this was a keyboard or a mouse binding.
    , inputType      :: InputType
    }
    deriving ( Show, Eq, Generic )

instance FromJSON BindingDetails where
    parseJSON = genericParseJSON snakeCaseFieldOptions

-- | Binding input type
data InputType = Keyboard | Mouse
    deriving ( Show, Eq, Generic )

instance FromJSON InputType where
    parseJSON = genericParseJSON defaultOptions
                                 { constructorTagModifier = (toLower <$>)
                                 }

-- | Triggered when the connection to the ipc is about to shutdown because of a user
-- action such as a restart or exit command
data ShutdownEvent = ShutdownEvent
    { change :: ShutdownChange -- ^ Why the IPC is shutting down
    }
    deriving ( Show, Eq, Generic )

instance FromJSON ShutdownEvent where
    parseJSON = genericParseJSON defaultOptions

-- | Reason for IPC shutdown
data ShutdownChange = Restart | Exit
    deriving ( Show, Eq, Generic )

instance FromJSON ShutdownChange where
    parseJSON = genericParseJSON defaultOptions
                                 { constructorTagModifier = (toLower <$>)
                                 }

-- | Triggered by a subscription to tick events or by a SEND_TICK message
data TickEvent = TickEvent
    { first   :: Bool
    , payload :: Text -- ^ @SEND_TICK@ with a payload of arbitrary string, or empty otherwise
    }
    deriving ( Show, Eq, Generic )

instance FromJSON TickEvent where
    parseJSON = genericParseJSON defaultOptions

makeFieldLabelsWith noPrefixFieldLabels ''WorkspaceEvent

makeFieldLabelsWith noPrefixFieldLabels ''WindowEvent

makeFieldLabelsWith noPrefixFieldLabels ''BindingEvent

makeFieldLabelsWith noPrefixFieldLabels ''BindingDetails

makeFieldLabelsWith noPrefixFieldLabels ''ShutdownEvent

makeFieldLabelsWith noPrefixFieldLabels ''TickEvent

makePrismLabels ''Event

makePrismLabels ''WorkspaceChange

makePrismLabels ''OutputEvent

makePrismLabels ''ModeEvent

makePrismLabels ''WindowChange

makePrismLabels ''InputType

makePrismLabels ''ShutdownChange
