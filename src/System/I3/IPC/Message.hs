-- | Sending one-time messages to i3.
-- This module should be imported qualified if
-- working with other IPC modules
module System.I3.IPC.Message
    ( send
    , module System.I3.IPC.Message.Types
    , module System.I3.IPC.Message.Reply
    ) where

import           Control.Monad.IO.Class      ( MonadIO, liftIO )

import qualified Network.Socket.ByteString   as Socket ( send )

import           System.I3.IPC.Message.Reply
                 ( BarConfig
                 , BindingModeReply
                 , BindingStateReply
                 , ConfigReply
                 , MarkReply
                 , Output
                 , OutputReply
                 , Reply(..)
                 , Result
                 , VersionReply
                 , Workspace
                 , WorkspaceReply
                 , decodeReply
                 )
import           System.I3.IPC.Message.Types
                 ( Message(Message)
                 , MessageType(..)
                 , message
                 )
import           System.I3.IPC.Socket        ( I3Socket(I3Socket) )

-- | Send message to i3
send :: (MonadIO m) => I3Socket -> MessageType -> m Int
send (I3Socket soc) msgType = liftIO $ Socket.send soc msg
  where
    (Message msg) = message msgType
