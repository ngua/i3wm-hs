{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE StrictData #-}

{-# OPTIONS_HADDOCK prune #-}

-- | Types representing i3's message replies, for those not already described
-- in `System.I3.IPC.Types`. This module should be imported qualified if imported
-- directly. Alternately, "System.I3.IPC.Message" re-exports the type constructors
-- contained herein.
module System.I3.IPC.Message.Reply
    ( decodeReply
    , Reply(..)
    , BarConfig
    , Result(..)
    , Workspace(..)
    , WorkspaceReply(..)
    , OutputReply(..)
    , Output(..)
    , MarkReply(..)
    , Mark
    , VersionReply(..)
    , ConfigReply(..)
    , BindingModeReply(..)
    , BindingStateReply(..)
    , BarConfigReply(..)
    , BindingMode
    , Config
    ) where

import           Control.Monad       ( MonadPlus(mzero) )

import           Data.Aeson          hiding ( Result )
import           Data.Bifunctor      ( Bifunctor(bimap) )
import           Data.ByteString     ( ByteString )
import           Data.Sequence       ( Seq )
import           Data.Text           ( Text )

import           GHC.Generics        ( Generic )

import           Optics

import           Prelude             hiding ( id )

import           System.I3.IPC.Error ( IpcError
                                     , IpcError(SerializationError)
                                     , fromString
                                     )
import           System.I3.IPC.Types

-- | The reply format is identical to the normal message format. There also is the
-- magic string, then the message length, then the message type and the payload
data Reply
    = -- | Confirmation/Error code for the @RUN_COMMAND@ message.
      Command [Result]
      -- | Reply to the @GET_WORKSPACES@ message.
    | Workspaces WorkspaceReply
      -- | Confirmation/Error code for the @SUBSCRIBE@ message.
    | Subscription Result
      -- | Reply to the @GET_OUTPUTS@ message.
    | Outputs OutputReply
      -- | Reply to the @GET_TREE@ message.
    | Tree Node
      -- | Reply to the @GET_MARKS@ message.
    | Marks MarkReply
      -- | Reply to the @GET_BAR_CONFIG@ message.
    | BarConfig BarConfigReply
      -- | Reply to the @GET_VERSION@ message.
    | Version VersionReply
      -- | Reply to the @GET_BINDING_MODES@ message.
    | BindingModes BindingModeReply
      -- | Reply to the @GET_CONFIG@ message.
    | Config ConfigReply
      -- | Reply to the @SEND_TICK@ message.
    | Tick Result
      -- | Reply to the @GET_BINDING_STATE@ message.
    | BindingState BindingStateReply
    deriving ( Show, Eq, Generic )

-- | Strict decoding of bytestring response into `Reply`
decodeReply :: Int -> ByteString -> Either IpcError Reply
decodeReply 0 bs  = bimap fromString Command (eitherDecodeStrict' bs)
decodeReply 1 bs  = bimap fromString Workspaces (eitherDecodeStrict' bs)
decodeReply 2 bs  = bimap fromString Subscription (eitherDecodeStrict' bs)
decodeReply 3 bs  = bimap fromString Outputs (eitherDecodeStrict' bs)
decodeReply 4 bs  = bimap fromString Tree (eitherDecodeStrict' bs)
decodeReply 5 bs  = bimap fromString Marks (eitherDecodeStrict' bs)
decodeReply 6 bs  = bimap fromString BarConfig (eitherDecodeStrict' bs)
decodeReply 7 bs  = bimap fromString Version (eitherDecodeStrict' bs)
decodeReply 8 bs  = bimap fromString BindingModes (eitherDecodeStrict' bs)
decodeReply 9 bs  = bimap fromString Config (eitherDecodeStrict' bs)
decodeReply 10 bs = bimap fromString Tick (eitherDecodeStrict' bs)
-- @GET_BINDING_STATE@ is notably 12, not 11
decodeReply 12 bs = bimap fromString BindingState (eitherDecodeStrict' bs)
decodeReply _ _   = Left . SerializationError $ "Unknown reply"

-- | Encodes either a 'BarConfig' object, or a list of string IDs
-- of all configured bars
data BarConfigReply = BarConfigObj BarConfig | BarIDs [Name]
    deriving ( Show, Eq, Generic )

instance FromJSON BarConfigReply where
    parseJSON o@(Object _) = BarConfigObj <$> parseJSON o
    parseJSON a@(Array _)  = BarIDs <$> parseJSON a
    parseJSON _            = mzero

-- | Success for parsing a given command, with optional error message
data Result = Result { success :: Bool, parseError :: Maybe Bool }
    deriving ( Show, Eq, Generic )

instance FromJSON Result where
    parseJSON = genericParseJSON snakeCaseFieldOptions

-- | A serialized list of workspaces
newtype WorkspaceReply = WorkspaceReply { items :: Seq Workspace }
    deriving ( Show, Eq, Generic, FromJSON )

-- | An i3 workspace
data Workspace = Workspace
    { -- | The internal ID (actually a C pointer value) of this container. Do not make
      -- any assumptions about it. You can use it to (re-)identify and address containers
      -- when talking to i3.
      id      :: ID
      -- | The logical number of the workspace. Corresponds to the command to switch to
      -- this workspace. For named workspaces, this will be -1.
    , num     :: Int
      -- | The name of this workspace if changed by the user, otherwise defaults to the
      -- string representation of the num field). Encoded in UTF-8.
    , name    :: Text
      -- | Whether this workspace is currently visible on an output (multiple workspaces can be
      -- visible at the same time).
    , visible :: Bool
      -- | Whether this workspace currently has the focus (only one workspace can have the
      -- focus at the same time).
    , focused :: Bool
      -- | Whether a window on this workspace has the "urgent" flag set.
    , urgent  :: Bool
      -- | The rectangle of this workspace (equals the rect of the output it is on), consists
      -- of x, y, width, height.
    , rect    :: Rect
      -- | The video output this workspace is on (LVDS1, VGA1, …).
    , output  :: Text
    }
    deriving ( Show, Eq, Generic )

instance FromJSON Workspace where
    parseJSON = genericParseJSON defaultOptions

-- | A serialized list of outputs
newtype OutputReply = OutputReply { items :: Seq Output }
    deriving ( Show, Eq, Generic, FromJSON )

-- | An i3 output
data Output = Output
    { -- | The name of this output (as seen in xrandr(1)). Encoded in UTF-8.
      name             :: Name
      -- | Whether this output is currently active (has a valid mode).
    , active           :: Bool
      -- | Whether this output is currently the primary output.
    , primary          :: Maybe Bool
      -- | The name of the current workspace that is visible on this output. null
      -- if the output is not active.
    , currentWorkspace :: Maybe Text
      -- | The rectangle of this output (equals the rect of the output it is on),
      -- consists of x, y, width, height.
    , rect             :: Rect
    }
    deriving ( Show, Eq, Generic )

instance FromJSON Output where
    parseJSON = genericParseJSON snakeCaseFieldOptions

type Mark = Name

-- | A single (unordered) array of strings for each container that has a mark. A mark
-- can only be set on one container, so the array is unique.
newtype MarkReply = MarkReply { items :: Seq Mark }
    deriving ( Show, Eq, Generic, FromJSON )

-- | Current i3 version
data VersionReply = VersionReply
    { -- | The major version of i3, such as 4.
      major                :: Int
      -- | The minor version of i3, such as 2. Changes in the IPC interface (new
      -- features) will only occur with new minor (or major) releases. However,
      -- bugfixes might be introduced in patch releases, too.
    , minor                :: Int
      -- | The patch version of i3, such as 1 (when the complete version is
      -- 4.2.1). For versions such as 4.2, patch will be set to 0.
    , patch                :: Int
      -- | A human-readable version of i3 containing the precise git version,
      -- build date and branch name. When you need to display the i3 version to
      -- your users, use the human-readable version whenever possible (since
      -- this is what i3 --version displays, too).
    , humanReadable        :: Text
      -- | The current config path
    , loadedConfigFileName :: FilePath -- TODO change this to a better filepath lib type
    }
    deriving ( Show, Eq, Generic )

instance FromJSON VersionReply where
    parseJSON = genericParseJSON snakeCaseFieldOptions

type BindingMode = Name

-- | Array of currently configured binding modes
newtype BindingModeReply = BindingModeReply { modes :: Seq BindingMode }
    deriving ( Show, Eq, Generic, FromJSON )

-- | Name of currently configured binding mode
data BindingStateReply = BindingStateReply { name :: Name }
    deriving ( Show, Eq, Generic )

instance FromJSON BindingStateReply where
    parseJSON = genericParseJSON defaultOptions

type Config = Text

-- | Path to the most recently loaded user config
data ConfigReply = ConfigReply { config :: Config }
    deriving ( Show, Eq, Generic )

instance FromJSON ConfigReply where
    parseJSON = genericParseJSON defaultOptions

makeFieldLabelsWith noPrefixFieldLabels ''Result

makeFieldLabelsWith noPrefixFieldLabels ''WorkspaceReply

makeFieldLabelsWith noPrefixFieldLabels ''Workspace

makeFieldLabelsWith noPrefixFieldLabels ''Output

makeFieldLabelsWith noPrefixFieldLabels ''OutputReply

makeFieldLabelsWith noPrefixFieldLabels ''VersionReply

makeFieldLabelsWith noPrefixFieldLabels ''BindingModeReply

makeFieldLabelsWith noPrefixFieldLabels ''BindingStateReply

makeFieldLabelsWith noPrefixFieldLabels ''ConfigReply

makeFieldLabelsWith noPrefixFieldLabels ''MarkReply

makePrismLabels ''Reply

makePrismLabels ''BarConfigReply
