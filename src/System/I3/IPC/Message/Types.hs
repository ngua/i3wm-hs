{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE StrictData #-}

-- | See https://i3wm.org/docs/ipc.html#_sending_messages_to_i3
module System.I3.IPC.Message.Types
    ( message
    , Message(Message)
    , ClientMessage(..)
    , MessageType(..)
    ) where

import           Control.Monad           ( unless )

import           Data.Aeson
import           Data.ByteString         ( ByteString )
import qualified Data.ByteString.Lazy    as LB
import           Data.Int                ( Int64 )
import           Data.Serialize.Put
import           Data.Text               ( Text )
import qualified Data.Text               as T
import qualified Data.Text.Lazy          as TL
import qualified Data.Text.Lazy.Encoding as TL

import           GHC.Generics            ( Generic )

import           Optics

import           System.I3.IPC.Subscribe ( Subscription )
import           System.I3.IPC.Types     ( ID, Name )

-- | 'Message' pattern
pattern Message :: ByteString -> Message
pattern Message unMessage <- MkMessage { unMessage }

-- | Wrapper for message containing a properly formatted binary-encoded payload
-- The inner 'ByteString' can be accessed through a read-only optic label:
--
-- > msg ^. #unMessage
--
-- or through pattern-matching:
--
-- > foo (Message m) = ...
--
newtype Message = MkMessage { unMessage :: ByteString }
    deriving ( Show, Eq, Generic )

-- | Currently implemented types for sending messages to i3
data MessageType
    = -- | Run the payload as an i3 command (like the commands you can bind to keys).
      -- TODO replace text with some defined command type
      RunCommand [Text]
      -- | Get the list of current workspaces.
    | GetWorkspaces
      -- |Subscribe this IPC connection to the event types specified.
    | Subscribe [Subscription]
      -- | Get the list of current outputs.
    | GetOutputs
      -- | Get the i3 layout tree
    | GetTree
      -- | Gets the names of all currently set marks.
    | GetMarks
      -- | Gets the specified bar configuration or the names of all bar configurations
      -- if payload is empty.
    | GetBarConfig (Maybe Name)
      -- | Get the i3 version
    | GetVersion
      -- | Get the names of all currently configured binding modes.
    | GetBindingModes
      -- | Get the name of the last loaded i3 config
    | GetConfig
      -- | Send a tick event with the specified payload.
    | SendTick Text
      -- |Sends an i3 sync event with the specified random value to the specified window.
    | Sync ClientMessage
      -- |Request the current binding state, i.e. the currently active binding mode name.
    | GetBindingState
    deriving ( Show, Eq )

-- | Create a client sync message for i3. @rnd@ should be a random integer value that
-- identifies the sync request. Read more: https://i3wm.org/docs/testsuite.html#i3_sync
data ClientMessage = ClientMessage
    { window :: ID -- ^ Window ID
    , rnd    :: Int -- ^ A random value
    }
    deriving ( Show, Eq, Generic )

instance ToJSON ClientMessage where
    toEncoding = genericToEncoding defaultOptions
    toJSON     = genericToJSON defaultOptions

type Length = Int64

type Type = Int

type Body = LB.ByteString

-- | Smart constructor for 'Message', creates payload according to i3 specification:
-- "i3-ipc" \<message length> \<message type> \<payload>
message :: MessageType -> Message
message ty = MkMessage { unMessage }
  where
    payload   = encodePayload ty

    unMessage =
        mkByteStringMessage (typeNumeric ty) (LB.length payload) payload

-- | Encode the message payload (if any)
encodePayload :: MessageType -> LB.ByteString
encodePayload (RunCommand command)       =
    TL.encodeUtf8 . TL.fromStrict . T.intercalate ";" $ command
encodePayload GetWorkspaces              = mempty
encodePayload (Subscribe ss)             = encode ss
encodePayload GetOutputs                 = mempty
encodePayload GetTree                    = mempty
encodePayload GetMarks                   = mempty
encodePayload (GetBarConfig Nothing)     = mempty
encodePayload (GetBarConfig (Just name)) = encode name
encodePayload GetVersion                 = mempty
encodePayload GetBindingModes            = mempty
encodePayload GetConfig                  = mempty
encodePayload (SendTick str)             = TL.encodeUtf8 . TL.fromStrict $ str
encodePayload (Sync _)                   = mempty
encodePayload GetBindingState            = mempty

-- | Create the formatted bytestring message
mkByteStringMessage :: Type -> Length -> Body -> ByteString
mkByteStringMessage ty len payload = runPut $ do
    putByteString magicString
    putWord32host (fromIntegral len)
    putWord32host (fromIntegral ty)
    unless (LB.null payload) (putLazyByteString payload)
  where
    magicString = "i3-ipc"

-- | Convert 'MessageType' to corresponding integer
typeNumeric :: (Integral a) => MessageType -> a
typeNumeric (RunCommand _)   = 0
typeNumeric GetWorkspaces    = 1
typeNumeric (Subscribe _)    = 2
typeNumeric GetOutputs       = 3
typeNumeric GetTree          = 4
typeNumeric GetMarks         = 5
typeNumeric (GetBarConfig _) = 6
typeNumeric GetVersion       = 7
typeNumeric GetBindingModes  = 8
typeNumeric GetConfig        = 9
typeNumeric (SendTick _)     = 10
typeNumeric (Sync _)         = 11
typeNumeric GetBindingState  = 12

makeFieldLabelsWith noPrefixFieldLabels ''ClientMessage

makePrismLabels ''MessageType

makeFieldLabelsWith (noPrefixFieldLabels & generateUpdateableOptics .~ False)
                    ''Message
