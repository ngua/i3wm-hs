{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE StrictData #-}

-- | Responses received from i3
module System.I3.IPC.Response ( Response(..) ) where

import           GHC.Generics            ( Generic )

import           Optics.TH               ( makePrismLabels )

import           System.I3.IPC.Message   ( Reply )
import           System.I3.IPC.Subscribe ( Event )

-- | Represents response from i3. The IPC has two communication mechanisms - one-time
-- replies and subscribed events. If the type of the response is not known, this
-- type can be used to represent either.
data Response
    = Message Reply -- ^ One-time messages to i3
    | Subscribe Event -- ^ Responses for subscribed events
    deriving ( Show, Eq, Generic )

makePrismLabels ''Response
