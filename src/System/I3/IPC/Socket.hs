{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE PatternSynonyms #-}

-- |
module System.I3.IPC.Socket
    ( I3Socket(I3Socket)
    , i3SocketPath
    , connectIPC
    , closeIPC
    ) where

import           Control.Monad.Catch       ( MonadThrow )
import           Control.Monad.Catch.Pure  ( MonadThrow(throwM) )
import           Control.Monad.IO.Class    ( MonadIO, liftIO )

import           Data.ByteString           ( ByteString )
import qualified Data.ByteString.Char8     as C

import           GHC.Generics              ( Generic )

import           Network.Socket

import           Optics

import           System.Environment        ( lookupEnv )
import           System.Exit               ( ExitCode(ExitSuccess) )
import           System.I3.IPC.Error       ( IpcError(ConnectionError) )
import           System.Process.ByteString ( readProcessWithExitCode )

-- | Pattern for 'I3Socket'
pattern I3Socket :: Socket -> I3Socket
pattern I3Socket getSocket <- MkI3Socket { getSocket }

{-# COMPLETE I3Socket :: I3Socket #-}

-- | Newtype for i3 IPC Unix socket
-- The inner socket can be accessed either through a read-only optic label:
--
-- > soc ^. #getSocket
--
-- or by pattern-matching:
--
-- > foo (I3Socket soc) = ...
--
newtype I3Socket = MkI3Socket { getSocket :: Socket }
    deriving ( Show, Eq, Generic )

-- | Find the path for i3's IPC unix socket, first trying the environment
-- variable @I3SOCK@, or by calling i3 directly if it has not been set
i3SocketPath :: (MonadIO m, MonadThrow m) => m ByteString
i3SocketPath = liftIO
    $ lookupEnv "I3SOCK" >>= maybe callI3 return . (C.pack <$>)
  where
    callI3 = do
        (exitCode, out, _) <- liftIO
            $ readProcessWithExitCode "i3" [ "--get-socketpath" ] mempty
        if exitCode == ExitSuccess
            then return . C.filter ('\n' /=) $ out
            else throwM $ ConnectionError "Couldn't get i3 socket path"

-- | Establish new connection, producing an i3 IPC socket
connectIPC :: (MonadIO m, MonadThrow m) => m I3Socket
connectIPC = liftIO i3SocketPath >>= connectI3
  where
    connectI3 path = liftIO $ socket AF_UNIX Stream 0 >>= \soc ->
        connect soc (SockAddrUnix . C.unpack $ path) >> return (MkI3Socket soc)

-- | Close the 'I3Socket'
closeIPC :: (MonadIO m, MonadThrow m) => I3Socket -> m ()
closeIPC (I3Socket soc) = liftIO $ close soc

makeFieldLabelsWith (noPrefixFieldLabels & generateUpdateableOptics .~ False)
                    ''I3Socket
