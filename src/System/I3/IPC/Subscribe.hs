{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE StrictData #-}

-- | Subscribe to IPC events
-- This module should be imported qualified if
-- working with other IPC modules
module System.I3.IPC.Subscribe
    ( Subscription(..)
    , module System.I3.IPC.Subscribe.Event
    ) where

import           Data.Aeson
                 ( Options(constructorTagModifier)
                 , ToJSON(toJSON, toEncoding)
                 , defaultOptions
                 , genericToEncoding
                 , genericToJSON
                 )
import           Data.Aeson.Casing             ( snakeCase )

import           GHC.Generics                  ( Generic )

import           Optics.TH                     ( makePrismLabels )

-- import           System.I3.IPC.Event    ( Event )
import           System.I3.IPC.Subscribe.Event
                 ( BarconfigEvent
                 , BindingDetails
                 , BindingEvent
                 , Event(..)
                 , ModeEvent
                 , OutputEvent
                 , ShutdownEvent
                 , TickEvent
                 , WindowChange
                 , WindowEvent
                 , WorkspaceChange
                 , WorkspaceEvent
                 , decodeEvent
                 )

-- | Available event types for subscription
data Subscription
    = -- | Sent when the user switches to a different workspace, when a new workspace is
      -- initialized or when a workspace is removed (because the last client vanished).
      Workspace
      -- | Sent when RandR issues a change notification (of either screens, outputs,
      -- CRTCs or output properties).
    | Output
      -- | Sent whenever i3 changes its binding mode.
    | Mode
      -- | Sent when a client’s window is successfully reparented (that is when i3 has
      -- finished fitting it into a container), when a window received input focus or
      -- when certain properties of the window have changed.
    | Window
      -- | Sent when the hidden_state or mode field in the barconfig of any bar instance
      -- was updated and when the config is reloaded.
    | BarconfigUpdate
      -- | Sent when a configured command binding is triggered with the keyboard or mouse
    | Binding
      -- | Sent when the ipc shuts down because of a restart or exit by user command
    | Shutdown
      -- | Sent when the ipc client subscribes to the tick event (with "first": true)
      -- or when any ipc client sends a SEND_TICK message (with "first": false).
    | Tick
    deriving ( Show, Eq, Ord, Enum, Generic )

instance ToJSON Subscription where
    toJSON     =
        genericToJSON defaultOptions { constructorTagModifier = snakeCase }
    toEncoding =
        genericToEncoding defaultOptions { constructorTagModifier = snakeCase }

makePrismLabels ''Subscription
