{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE StrictData #-}

-- | Events generated in response to subscriptions
-- This module should be imported qualified if imported directly. Alternately,
-- "System.I3.IPC.Subscribe" re-exports the type constructors contained herein.
module System.I3.IPC.Subscribe.Event
    ( Event(..)
    , WorkspaceEvent(..)
    , WorkspaceChange(..)
    , OutputEvent(..)
    , ModeEvent(..)
    , WindowChange(..)
    , WindowEvent(..)
    , BarconfigEvent
    , BindingEvent(..)
    , BindingDetails(..)
    , ShutdownEvent(..)
    , ShutdownChange(..)
    , TickEvent(..)
    , InputType(..)
    , decodeEvent
    ) where

import           Data.Aeson
import           Data.Aeson.Casing   ( snakeCase )
import           Data.Bifunctor      ( Bifunctor(bimap) )
import           Data.ByteString     ( ByteString )
import           Data.Char
import           Data.List.Extra     ( dropEnd )
import           Data.Sequence       ( Seq )
import           Data.Text           ( Text )

import           GHC.Generics        ( Generic )

import           Optics

import           Prelude             hiding ( id )

import           System.I3.IPC.Error ( IpcError
                                     , IpcError(SerializationError)
                                     , fromString
                                     )
import           System.I3.IPC.Types

-- | Decode an IPC response into an 'Event'
decodeEvent :: Int -> ByteString -> Either IpcError Event
decodeEvent 0 bs = bimap fromString Workspaces (eitherDecodeStrict' bs)
decodeEvent 1 bs = bimap fromString Outputs (eitherDecodeStrict' bs)
decodeEvent 2 bs = bimap fromString Modes (eitherDecodeStrict' bs)
decodeEvent 3 bs = bimap fromString Windows (eitherDecodeStrict' bs)
decodeEvent 4 bs = bimap fromString BarconfigUpdates (eitherDecodeStrict' bs)
decodeEvent 5 bs = bimap fromString Bindings (eitherDecodeStrict' bs)
decodeEvent 6 bs = bimap fromString Shutdowns (eitherDecodeStrict' bs)
decodeEvent 7 bs = bimap fromString Ticks (eitherDecodeStrict' bs)
decodeEvent _ _  = Left . SerializationError $ "Unknown event"

-- | Available event types for subscription
data Event
    = -- | Sent when the user switches to a different workspace, when a new workspace is
      -- initialized or when a workspace is removed (because the last client vanished).
      Workspaces WorkspaceEvent
      -- | Sent when RandR issues a change notification (of either screens, outputs,
      -- CRTCs or output properties).
    | Outputs OutputEvent
      -- | Sent whenever i3 changes its binding mode.
    | Modes ModeEvent
      -- | Sent when a client’s window is successfully reparented (that is when i3 has
      -- finished fitting it into a container), when a window received input focus or
      -- when certain properties of the window have changed.
    | Windows WindowEvent
      -- | Sent when the hidden_state or mode field in the barconfig of any bar instance
      -- was updated and when the config is reloaded.
    | BarconfigUpdates BarconfigEvent
      -- | Sent when a configured command binding is triggered with the keyboard or mouse
    | Bindings BindingEvent
      -- | Sent when the ipc shuts down because of a restart or exit by user command
    | Shutdowns ShutdownEvent
      -- | Sent when the ipc client subscribes to the tick event (with "first": true)
      -- or when any ipc client sends a @SEND_TICK@ message (with "first": false).
    | Ticks TickEvent
    deriving ( Show, Eq, Generic )

instance FromJSON Event where
    parseJSON = genericParseJSON defaultOptions
                                 { constructorTagModifier = stripSuffix
                                 }
      where
        stripSuffix = snakeCase . dropEnd 1

-- | This event consists of a single serialized map containing a property change (string)
-- which indicates the type of the change ("focus", "init", "empty", "urgent", "reload",
-- "rename", "restored", "move"). A current (object) property will be present with the
-- affected workspace whenever the type of event affects a workspace (otherwise, it will be null).
-- When the change is "focus", an old (object) property will be present with the previous workspace.
-- When the first switch occurs (when i3 focuses the workspace visible at the beginning) there
-- is no previous workspace, and the old property will be set to null. Also note that if the previous
-- is empty it will get destroyed when switching, but will still be present in the "old" property.
data WorkspaceEvent = WorkspaceEvent
    { change  :: WorkspaceChange
    , current :: Node
    , old     :: Maybe Node
    }
    deriving ( Show, Eq, Generic )

instance FromJSON WorkspaceEvent where
    parseJSON = genericParseJSON defaultOptions

-- | The type of workspace change
data WorkspaceChange
    = FocusWS
    | InitWS
    | EmptyWS
    | UrgentWS
    | ReloadWS
    | RenameWS
    | RestoredWS
    | MoveWS
    deriving ( Show, Eq, Generic )

instance FromJSON WorkspaceChange where
    parseJSON = genericParseJSON defaultOptions
                                 { constructorTagModifier = stripSuffix
                                 }
      where
        stripSuffix = snakeCase . dropEnd 2

-- | Consists of a single [...] property, change (string), which indicates the type of
-- the change (currently only "unspecified").
data OutputEvent = OutputEvent { change :: Text }
    deriving ( Show, Eq, Generic )

instance FromJSON OutputEvent where
    parseJSON = genericParseJSON defaultOptions

-- | Name of current mode in use
data ModeEvent = ModeEvent
    { -- | Name of mode, the same as specified in config when creating a mode.
      -- The default mode is simply named default.
      mode        :: Name
      -- | Whether pango markup shall be used for displaying this mode
    , pangoMarkup :: Bool
    }
    deriving ( Show, Eq, Generic )

instance FromJSON ModeEvent where
    parseJSON = genericParseJSON snakeCaseFieldOptions

-- | Consists of a single [..] property, change (string), which indicates the type of the change
data WindowEvent = WindowEvent
    { change    :: WindowChange
    , container :: Node -- The window's parent container
    }
    deriving ( Show, Eq, Generic )

instance FromJSON WindowEvent where
    parseJSON = genericParseJSON defaultOptions

-- | Type of window change
data WindowChange
    = New -- ^ Window has become managed by i3
    | Close -- ^ Window has closed
    | Focus -- ^ Window has has received input focus
    | Title -- ^ Window’s title has changed
    | FullscreenMode -- ^ Window has entered or exited fullscreen mode
    | Move -- ^ Window has changed its position in the tree
    | Floating -- ^ Window has transitioned to or from floating
    | Urgent -- ^ Window has become urgent or lost its urgent status
    | Mark -- ^ A mark has been added to or removed from the window
    deriving ( Show, Eq, Generic )

instance FromJSON WindowChange where
    parseJSON = genericParseJSON snakeCaseConstructorOptions

-- | Options from the barconfig of the specified @bar_id@ that were updated in i3. This
-- event is the same as a @GET_BAR_CONFIG@ reply for the bar with the given id.
type BarconfigEvent = BarConfig

-- | Details of a binding that ran a command because of user input
data BindingEvent = BindingEvent
    { -- | Indicates what sort of binding event was triggered (right now it will always
      -- be "run" but may be expanded in the future).
      change  :: Text
      -- | Details about the binding that was run
    , binding :: BindingDetails
    }
    deriving ( Show, Eq, Generic )

instance FromJSON BindingEvent where
    parseJSON = genericParseJSON defaultOptions

-- | Mouse or keyboard binding that was run
data BindingDetails = BindingDetails
    { -- | The i3 command that is configured to run for this binding.
      command        :: Text
      -- | The group and modifier keys that were configured with this binding.
    , eventStateMask :: Seq Text
      -- | If the binding was configured with bindcode, this will be the key code that
      -- was given for the binding. If the binding is a mouse binding, it will be the
      -- number of the mouse button that was pressed. Otherwise it will be 0.
    , inputCode      :: Int
      -- | If this is a keyboard binding that was configured with bindsym, this field
      -- will contain the given symbol. Otherwise it will be null.
    , symbol         :: Maybe Text
      -- | This will be "keyboard" or "mouse" depending on whether or not this was a keyboard or a mouse binding.
    , inputType      :: InputType
    }
    deriving ( Show, Eq, Generic )

instance FromJSON BindingDetails where
    parseJSON = genericParseJSON snakeCaseFieldOptions

-- | Binding input type
data InputType = Keyboard | Mouse
    deriving ( Show, Eq, Generic )

instance FromJSON InputType where
    parseJSON = genericParseJSON defaultOptions
                                 { constructorTagModifier = (toLower <$>)
                                 }

-- | Triggered when the connection to the ipc is about to shutdown because of a user
-- action such as a restart or exit command
data ShutdownEvent = ShutdownEvent
    { change :: ShutdownChange -- ^ Why the IPC is shutting down
    }
    deriving ( Show, Eq, Generic )

instance FromJSON ShutdownEvent where
    parseJSON = genericParseJSON defaultOptions

-- | Reason for IPC shutdown
data ShutdownChange = Restart | Exit
    deriving ( Show, Eq, Generic )

instance FromJSON ShutdownChange where
    parseJSON = genericParseJSON defaultOptions
                                 { constructorTagModifier = (toLower <$>)
                                 }

-- | Triggered by a subscription to tick events or by a @SEND_TICK@ message
data TickEvent = TickEvent
    { first   :: Bool
    , payload :: Text -- ^ @SEND_TICK@ with a payload of arbitrary string, or empty otherwise
    }
    deriving ( Show, Eq, Generic )

instance FromJSON TickEvent where
    parseJSON = genericParseJSON defaultOptions

makeFieldLabelsWith noPrefixFieldLabels ''WorkspaceEvent

makeFieldLabelsWith noPrefixFieldLabels ''WindowEvent

makeFieldLabelsWith noPrefixFieldLabels ''BindingEvent

makeFieldLabelsWith noPrefixFieldLabels ''BindingDetails

makeFieldLabelsWith noPrefixFieldLabels ''ShutdownEvent

makeFieldLabelsWith noPrefixFieldLabels ''TickEvent

makeFieldLabelsWith noPrefixFieldLabels ''OutputEvent

makeFieldLabelsWith noPrefixFieldLabels ''ModeEvent

makePrismLabels ''Event

makePrismLabels ''WorkspaceChange

makePrismLabels ''WindowChange

makePrismLabels ''InputType

makePrismLabels ''ShutdownChange
