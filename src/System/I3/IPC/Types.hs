-- | Types common to different IPC mechanisms
module System.I3.IPC.Types ( module System.I3.IPC.Types.Internal ) where

import           System.I3.IPC.Types.Internal
                 ( BarColor
                 , BarConfig
                 , BarMode
                 , BarPosition
                 , Border
                 , FullscreenMode
                 , ID(..)
                 , Layout
                 , Name(..)
                 , Node
                 , NodeType
                 , Rect
                 , WindowProperty
                 , WindowType
                 , snakeCaseConstructorOptions
                 , snakeCaseFieldOptions
                 )
