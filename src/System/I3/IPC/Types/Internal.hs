{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}

-- | Internal module for types relevant to different IPC facilities
module System.I3.IPC.Types.Internal
    ( Node(..)
    , Rect(..)
    , ID(..)
    , Name(..)
    , NodeType(..)
    , Border(..)
    , Layout(..)
    , WindowProperty(..)
    , WindowType(..)
    , FullscreenMode(..)
    , BarConfig(..)
    , BarPosition(..)
    , BarMode(..)
    , BarColor(..)
    , snakeCaseFieldOptions
    , snakeCaseConstructorOptions
    ) where

import           Control.Monad     ( MonadPlus(mzero) )

import           Data.Aeson
import           Data.Aeson.Casing ( snakeCase )
import           Data.Char         ( toLower )
import           Data.List.Extra   ( dropEnd )
import           Data.Map          ( Map )
import           Data.Sequence     ( Seq )
import           Data.Text         ( Text )

import           GHC.Generics      ( Generic )

import           Optics

import           Prelude           hiding ( id )

-- | Convert snake_case JSON field names
snakeCaseFieldOptions :: Options
snakeCaseFieldOptions = defaultOptions { fieldLabelModifier = snakeCase }

-- | Convert snake_case JSON names to sum type constructors
snakeCaseConstructorOptions :: Options
snakeCaseConstructorOptions =
    defaultOptions { constructorTagModifier = snakeCase }

-- | Wrapper for "id" fields in messages/replies
newtype ID = ID Int
    deriving ( Show, Eq, Generic, ToJSON, FromJSON )

instance Field1 ID ID Int Int

-- | Wrapper for "name" fields in messages/replies
newtype Name = Name Text
    deriving ( Show, Eq, Generic, ToJSON, FromJSON )

instance Field1 Name Name Text Text

-- | Geometric coordinates for different elements
data Rect = Rect { x :: Int, y :: Int, width :: Int, height :: Int }
    deriving ( Show, Eq, Generic )

instance FromJSON Rect where
    parseJSON = genericParseJSON defaultOptions

-- | From https://i3wm.org/docs/ipc.html#_tree_reply:
-- Each node [...] has at least the properties listed below. While the nodes might have more
-- properties, please do not use any properties which are not documented here. They are not
-- yet finalized and will probably change!
data Node = Node
    { -- | The internal ID (actually a C pointer value) of this container. Do not make any
      -- assumptions about it. You can use it to (re-)identify and address containers when talking to i3.
      id                 :: ID
      -- | The internal name of this container. For all containers which are part of the tree structure
      -- down to the workspace contents, this is set to a nice human-readable name of the container.
      -- For containers that have an X11 window, the content is the title (@_NET_WM_NAME@ property) of
      -- that window. For all other containers, the content is not defined (yet).
    , name               :: Maybe Name
      -- | Type of this container. Can be one of "root", "output", "con", "floating_con", "workspace" or "dockarea".
    , type'              :: NodeType
      -- | Can be either "normal", "none" or "pixel", depending on the container’s border style.
    , border             :: Border
      -- | Number of pixels of the border width.
    , currentBorderWidth :: Int
      -- | Can be either "splith", "splitv", "stacked", "tabbed", "dockarea" or "output". Other values might
      -- be possible in the future, should [i3] add new layouts.
    , layout             :: Layout
      -- | The percentage which this container takes in its parent. A value of null means that the
      -- percent property does not make sense for this container, for example for the root container.
    , percent            :: Maybe Float
      -- | The absolute display coordinates for this container. Display coordinates means that when
      -- you have two 1600x1200 monitors on a single X11 Display (the standard way), the coordinates of
      -- the first window on the second monitor are { "x": 1600, "y": 0, "width": 1600, "height": 1200 }.
    , rect               :: Rect
      -- | The coordinates of the actual client window inside its container. These coordinates are
      -- relative to the container and do not include the window decoration (which is actually rendered
      -- on the parent container). So, when using the default layout, you will have a 2 pixel border
      -- on each side, making the window_rect { "x": 2, "y": 0, "width": 632, "height": 366 } (for example).
    , windowRect         :: Rect
      -- | The coordinates of the window decoration inside its container. These coordinates are relative
      -- to the container and do not include the actual client window.
    , decoRect           :: Rect
      -- | The original geometry the window specified when i3 mapped it. Used when switching a window to
      -- floating mode, for example.
    , geometry           :: Maybe Rect
      -- | The X11 window ID of the actual client window inside this container. This field is set to
      -- null for split containers or otherwise empty containers. This ID corresponds to what xwininfo(1)
      -- and other X11-related tools display (usually in hex).
    , window             :: Maybe ID
      -- | This optional field contains all available X11 window properties from the following
      -- list: title, instance, class, window_role and transient_for.
    , windowProperties   :: Maybe (Map WindowProperty (Maybe Text))
      -- | The window type (@_NET_WM_WINDOW_TYPE@). Possible values are undefined, normal, dialog,
      -- utility, toolbar, splash, menu, dropdown_menu, popup_menu, tooltip and notification.
    , windowType         :: Maybe WindowType
      -- | Whether this container (window, split container, floating container or workspace)
      -- has the urgency hint set, directly or indirectly. All parent containers up until the
      -- workspace container will be marked urgent if they have at least one urgent child.
    , urgent             :: Bool
      -- | List of marks assigned to container
    , marks              :: Seq Text
      -- | Whether this container is currently focused.
    , focused            :: Bool
      -- | List of child node IDs (see nodes, floating_nodes and id) in focus order.
      -- Traversing the tree by following the first entry in this array will result in
      -- eventually reaching the one node with focused set to true.
    , focus              :: Seq ID
      -- | Whether this container is in fullscreen state or not. Possible values are
      -- 0 (no fullscreen), 1 (fullscreened on output) or 2 (fullscreened globally).
      -- Note that all workspaces are considered fullscreened on their respective output.
    , fullscreenMode     :: FullscreenMode
      -- | The tiling (i.e. non-floating) child containers of this node.
    , nodes              :: Seq Node
      -- | The floating child containers of this node. Only non-empty on nodes with type workspace.
    , floatingNodes      :: Seq Node
    }
    deriving ( Show, Eq, Generic )

instance FromJSON Node where
    parseJSON =
        genericParseJSON defaultOptions
                         { fieldLabelModifier = snakeCase . filter ('\'' /=)
                         }

-- | Types of containers
data NodeType
    = RootNode
    | OutputNode
    | ConNode
    | FloatingConNode
    | WorkspaceNode
    | DockareaNode
    deriving ( Show, Eq, Generic )

instance FromJSON NodeType where
    parseJSON = genericParseJSON defaultOptions
                                 { constructorTagModifier = removeSuffix
                                 }
      where
        removeSuffix = snakeCase . dropEnd 4

-- | Container border style
data Border = Normal | None | Pixel
    deriving ( Show, Eq, Generic )

instance FromJSON Border where
    parseJSON = genericParseJSON defaultOptions
                                 { constructorTagModifier = (toLower <$>)
                                 }

-- | Layouts
data Layout
    = SplitH
    | SplitV
    | Stacked
    | Tabbed
    | DockArea
    | Output
    | Unknown -- ^ represents any new layout that i3 might introduce
    deriving ( Show, Eq, Generic )

instance FromJSON Layout where
    parseJSON = withText "Layout" $ \case
        "splith"   -> pure SplitH
        "splitv"   -> pure SplitV
        "stacked"  -> pure Stacked
        "tabbed"   -> pure Tabbed
        "dockArea" -> pure DockArea
        "output"   -> pure Output
        _          -> pure Unknown

-- | X11 window properties
data WindowProperty = Title | Instance | Class | WindowRole | TransientFor
    deriving ( Show, Eq, Ord, Generic )

instance FromJSON WindowProperty where
    parseJSON =
        genericParseJSON defaultOptions { constructorTagModifier = snakeCase }

instance FromJSONKey WindowProperty where
    fromJSONKey =
        genericFromJSONKey defaultJSONKeyOptions { keyModifier = snakeCase }

-- | Window type (@_NET_WM_WINDOW_TYPE@)
data WindowType
    = UndefinedWin
    | NormalWin
    | DialogWin
    | UtilityWin
    | ToolbarWin
    | SplashWin
    | MenuWin
    | DropdownMenuWin
    | PopupMenuWin
    | TooltipWin
    | NotificationWin
    | UnknownWin
    deriving ( Show, Eq, Generic )

instance FromJSON WindowType where
    parseJSON =
        genericParseJSON defaultOptions { constructorTagModifier = dropSuffix }
      where
        dropSuffix = snakeCase . dropEnd 3

-- | Fullscreen state of container
data FullscreenMode = NoFS | OnOutput | Global
    deriving ( Show, Eq, Generic, Ord, Enum )

instance FromJSON FullscreenMode where
    parseJSON = withScientific "FullscreenMode" $ \case
        0 -> pure NoFS
        1 -> pure OnOutput
        2 -> pure Global
        _ -> mzero

-- | Can be used by third-party workspace bars [...] to get the bar block configuration from i3
data BarConfig = BarConfig
    { -- | The ID for this bar. Included in case you request multiple configurations
      -- and want to differentiate the different replies.
      -- NOTE this is a string id
      id                   :: Name
      -- | Either dock (the bar sets the dock window type) or hide (the bar does not
      -- show unless a specific key is pressed).
    , mode                 :: BarMode
      -- | Either bottom or top at the moment.
    , position             :: BarPosition
      -- |Command which will be run to generate a statusline. Each line on stdout of this
      -- command will be displayed in the bar. At the moment, no formatting is supported.
    , statusCommand        :: Text
      -- | The font to use for text on the bar.
    , font                 :: Text
      -- | Display workspace buttons or not? Defaults to true.
    , workspaceButtons     :: Bool
      -- | Display the mode indicator or not? Defaults to true.
    , bindingModeIndicator :: Bool
      -- | Should the bar enable verbose output for debugging? Defaults to false.
    , verbose              :: Bool
      -- | Contains key/value pairs of colors. Each value is a color code in hex,
      -- formatted #rrggbb (like in HTML).
    , colors               :: Map BarColor Text
    }
    deriving ( Show, Eq, Generic )

instance FromJSON BarConfig where
    parseJSON = genericParseJSON snakeCaseFieldOptions

-- | Possible bar modes
data BarMode
    = Dock -- ^ The bar sets the dock window type
    | Hide -- ^ The bar is hidden unless specific key is pressed
    deriving ( Show, Eq, Generic )

instance FromJSON BarMode where
    parseJSON = genericParseJSON defaultOptions
                                 { constructorTagModifier = (toLower <$>)
                                 }

-- | Possible bar positions
data BarPosition = Top | Bottom
    deriving ( Show, Eq, Generic )

instance FromJSON BarPosition where
    parseJSON = genericParseJSON defaultOptions
                                 { constructorTagModifier = (toLower <$>)
                                 }

-- | Bar colors that may currently be configured
data BarColor
    = Background
    | Statusline
    | Separator
    | FocusedBackground
    | FocusedStatusline
    | FocusedSeparator
    | FocusedWorkspaceText
    | FocusedWorkspaceBg
    | FocusedWorkspaceBorder
    | ActiveWorkspaceText
    | ActiveWorkspaceBg
    | ActiveWorkspaceBorder
    | InactiveWorkspaceText
    | InactiveWorkspaceBg
    | InactiveWorkspaceBorder
    | BindingWorkspaceText
    | BindingWorkspaceBg
    | BindingWorkspaceBorder
    | BindingMode
    | BindingModeBg
    | BindingModeBorder
    deriving ( Show, Eq, Ord, Generic )

instance FromJSON BarColor where
    parseJSON = genericParseJSON snakeCaseConstructorOptions

instance FromJSONKey BarColor where
    fromJSONKey =
        genericFromJSONKey defaultJSONKeyOptions { keyModifier = snakeCase }

makeFieldLabelsWith noPrefixFieldLabels ''ID

makeFieldLabelsWith noPrefixFieldLabels ''Name

makeFieldLabelsWith noPrefixFieldLabels ''Node

makeFieldLabelsWith noPrefixFieldLabels ''Rect

makeFieldLabelsWith noPrefixFieldLabels ''BarConfig

makePrismLabels ''NodeType

makePrismLabels ''Border

makePrismLabels ''Layout

makePrismLabels ''WindowProperty

makePrismLabels ''WindowType

makePrismLabels ''FullscreenMode

makePrismLabels ''BarColor

makePrismLabels ''BarPosition

makePrismLabels ''BarMode
