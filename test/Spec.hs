{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeApplications #-}

module Main where

import           Data.Aeson                   ( FromJSON, decode' )
import qualified Data.ByteString.Lazy         as LB ( readFile )
import           Data.Maybe                   ( isJust )

import           Optics.Core

import           System.I3.IPC                ( findFocusedNode, findNode )
import           System.I3.IPC.Message        ( OutputReply, WorkspaceReply )
import           System.I3.IPC.Subscribe      ( BindingEvent, WorkspaceEvent )
import           System.I3.IPC.Types          ( BarConfig, ID(ID), Node )
import           System.I3.IPC.Types.Internal ( NodeType(..) )

import           Test.Hspec

main :: IO ()
main = hspec $ do
    describe "Replies" $ do
        describe "Tree reply" $ do
            it "Decodes serialized IPC tree response" $ do
                t <- decodeHelper @Node "./test/reply/tree.json"
                t `shouldSatisfy` isJust

            it "Finds focused node" $ do
                t <- decodeHelper @Node "./test/reply/tree.json"
                (findFocusedNode <$> t) `shouldSatisfy` isJust

            it "Finds node given a predicate" $ do
                t <- decodeHelper @Node "./test/reply/tree.json"
                let pred = \t -> t ^. #type' == RootNode
                let rn = findNode pred =<< t
                rn `shouldSatisfy` isJust
                (rn ^? _Just % #id) `shouldBe` Just (ID 93936724239328)

        describe "Workspaces reply" $ do
            it "Decodes serialized IPC workspaces response" $ do
                ws <- decodeHelper @WorkspaceReply
                                   "./test/reply/workspaces.json"
                ws `shouldSatisfy` isJust

        describe "Outputs reply" $ do
            it "Decodes serialized IPC outputs response" $ do
                ws <- decodeHelper @OutputReply "./test/reply/outputs.json"
                ws `shouldSatisfy` isJust

        describe "Bar config reply" $ do
            it "Decodes serialized IPC bar configuration response" $ do
                ws <- decodeHelper @BarConfig "./test/reply/barconfig.json"
                ws `shouldSatisfy` isJust

    describe "Events" $ do
        describe "Binding event" $ do
            it "Decodes serialized IPC binding event" $ do
                b <- decodeHelper @BindingEvent "./test/event/binding.json"
                b `shouldSatisfy` isJust

        describe "Workspace event" $ do
            it "Decodes serialized IPC workspace event" $ do
                -- n <- LB.readFile "./test/event/workspace.json"
                b <- decodeHelper @WorkspaceEvent "./test/event/workspace.json"
                b `shouldSatisfy` isJust

decodeHelper :: FromJSON a => FilePath -> IO (Maybe a)
decodeHelper fp = LB.readFile fp >>= \n -> return $ decode' n
